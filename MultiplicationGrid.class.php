<?php
/*
 * Copyright 2015 Richie.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Description of MultiplicationGrid
 *
 * @author Richie
 */
class MultiplicationGrid {

    private $index;

    /**
     * A constructor which allows the user to define an index for the multiplication grid
     * 
     * @param type $index The max number on the multiplication grid. 1 to n
     */
    public function __construct($index = 12) {
        $this->index = $index;
    }

    /**
     * Displays the Grid as a Table given the users input
     */
    public function printTable() {
        echo "<table>";
        for ($y = 1; $y <= $this->index; $y++) {
            echo "<tr>";
            for ($x = 1; $x <= $this->index; $x++) {
                echo "<td>" . $x * $y . "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    /**
     * Displays the Grid as a Table given the users input
     */
    public function printGrid() {
        for ($y = 1; $y <= $this->index; $y++) {
            for ($x = 1; $x <= $this->index; $x++) {
                echo $x * $y . " ";
            }
            echo "<br />";
        }
    }

}
